main : lib.o main.o dict.o
	ld -o main lib.o main.o dict.o

main.o: colon.inc lib.inc words.inc
	nasm -f elf64 main.asm -o main.o

%.o: %.asm
	nasm -f elf64 $< -o $@

.PHONY : clean	
clean : 
	rm *.o
	rm main
