%define STDOUT 1
%define STDERR 2

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error_msg

section .text

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60
	syscall

error_exit:
    mov     rdi, 1
    mov     rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
	cmp byte[rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	mov rsi, STDOUT
    jmp printer


; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdi, STDOUT
	mov rdx, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 10
	jmp print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r8, rsp
	push 0
	mov rax, rdi

.loop:
	mov rdx, 0
	mov r9, 10
	div r9
	add rdx, '0'
	dec rsp
	mov [rsp], dl
	cmp rax, 0
	ja .loop

.end:
	mov rdi, rsp
	call print_string
	mov rsp, r8
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	mov rax, rdi
	cmp rax, 0
	jl .negative

.positive:
	mov rdi, rax
	call print_uint
	ret

.negative:
	push rax
	mov rdi, '-'
	call print_char
	pop rax
	neg rax
	mov rdi, rax
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	call string_length
	mov r9, rax
	call string_length
	cmp rax, r9
	jne .return_0

	dec rdi
	dec rsi
	xor rax, rax
	xor r8, r8

.loop:
	inc rdi
	inc rsi
	mov al, byte[rdi]
	mov r8b, byte[rsi]
	cmp rax, r8
	jne .return_0
	cmp rax, 0
	je .return_1
	jmp .loop

.return_0:
	xor rax, rax
	ret

.return_1:
	mov rax, 1
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	mov r9, rsi
	push rdi
	push rsi

.space_loop:
	call read_char
	cmp rax, 0x9
	je .space_loop
	cmp rax, 0xA
	je .space_loop
	cmp rax, 0x20
	je .space_loop

	pop rsi
	pop rdi
	xor r8, r8
	
.symbol_loop:
	cmp rax, 0
	jz .break
	cmp rax, 0x9
	je .break
	cmp rax, 0xA
	je .break
	cmp rax, 0x20
	je .break
	cmp r8, r9
	jae .over
	mov [rdi+r8], rax
	inc r8
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	jmp .symbol_loop

.break:
	mov rax, rdi
	mov rdx, r8
	ret

.over:
	xor rax, rax
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	dec rdi
	xor rax, rax
	xor rdx, rdx
	xor rcx, rcx

.loop:
	inc rdi
	mov al, byte[rdi]
	cmp rax, 0
	je .end
	cmp rax, '0'
	jb .end
	cmp rax, '9'
	ja .end
	sub rax, '0'
	push rax
	mov rax, rcx
	mov r9, 10
	push rdx
	mul r9
	pop rdx
	mov rcx, rax
	pop rax
	add rcx, rax
	inc rdx
	jmp .loop

.end:
	cmp rdx, 0
	je .empty
	mov rax, rcx
	ret

.empty:
	xor rax, rax
	xor rdx, rdx
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'
	je .negative

.positive:
	call parse_uint
	cmp rdx, 0
	je .empty
	ret

.negative:
	inc rdi
	call parse_uint
	cmp rdx, 0
	je .empty
	neg rax
	inc rdx
	ret

.empty:
	xor rax, rax
	xor rdx, rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	call string_length
	inc rax
	cmp rax, rdx
	ja .error
	mov r8, rax
	dec rdi
	dec rsi
	xor rcx, rcx

.loop:
	cmp rcx, r8
	je .break
	inc rdi
	inc rsi
	mov r9b, byte[rdi]
	mov byte[rsi], r9b
	inc rcx
	jmp .loop

.break:
	mov rax, r8
	ret

.error:
	xor rax, rax
	ret

print_error_msg:
    mov rsi, STDERR
	jmp printer

printer:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, STDERR
    mov rax, 1
    syscall
    ret