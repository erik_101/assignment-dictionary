%include "lib.inc"
%include "words.inc"

global _start

extern find_word

section .rodata
	msg_too_long: db 'The entered string is too long! The maximum length is 255 characters.', 0
	msg_not_found: db 'Nothing was found in the dictionary for this key!', 0

%define BUFFER_SIZE 256

section .text

_start:
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	mov rsi, BUFFER_SIZE
	call read_word
	cmp rax, 0
	jz .too_long_err

	mov rdi, rsp
	mov rsi, PREV_POINTER
	call find_word
	cmp rax, 0
	jz .not_found_err
	
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax

	mov rdi, rax
	call print_string
	call print_newline
    call exit

	.too_long_err:
		mov rdi, msg_too_long
		jmp .error

	.not_found_err:
		mov rdi, msg_not_found
		jmp .error

	.error:
		call print_error_msg
		call error_exit
