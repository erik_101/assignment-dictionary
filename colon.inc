%macro colon 2

	%ifid %2
		%ifdef PREV_POINTER
			%%previous: dq PREV_POINTER
		%else
			%%previous: dq 0
		%endif
		%define PREV_POINTER %%previous
	%else
		%fatal 'Illegal second argument'
	%endif

	%ifstr %1
		db %1, 0
		%2:
	%else
		%fatal 'Illegal first argument'
	%endif

%endmacro